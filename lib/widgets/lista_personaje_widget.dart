import 'package:flutter/material.dart';
import 'package:sinnotaria/model/personaje_model.dart';
import 'package:sinnotaria/utilidades/styles.dart';

Widget listaPersonajesWidget(
  BuildContext context,
  List<PersonajeModel> personaje,
) {
  double _width = MediaQuery.of(context).size.width;

  return ListView.builder(
      itemCount: personaje.length,
      itemBuilder: (BuildContext context, int index) {
        return GestureDetector(
          /*onTap: () => Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ProductoDetalle(producto: producto))),*/
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 20),
            child: Stack(
              children: [
                Container(
                  width: _width,
                  height: 350,
                  margin: EdgeInsets.symmetric(horizontal: 16),
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(24)),
                    child: FadeInImage(
                      fit: BoxFit.fill,
                      placeholder: AssetImage("assets/img/giphy.gif"),
                      image: NetworkImage(personaje[index].img),
                    ),
                  ),
                  // child:
                ),
                Opacity(
                  opacity: 0.7,
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 16),
                    width: _width,
                    height: 350,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(24),
                      gradient: LinearGradient(
                          colors: [
                            Colors.transparent,
                            Colors.transparent,
                            Colors.transparent,
                            blackColor,
                            backgrounColor0,
                          ],
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter),
                    ),
                  ),
                ),
                Container(
                  width: _width,
                  height: 350,
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          personaje[index].nickname,
                          style: TextStyle(
                              color: whiteColor,
                              fontWeight: FontWeight.bold,
                              fontSize: 24),
                        ),
                        Text(
                          personaje[index].name,
                          style: TextStyle(
                              color: whiteColor,
                              fontWeight: FontWeight.bold,
                              fontSize: 16),
                        ),
                        Text(
                          personaje[index].status,
                          style: TextStyle(
                              color: whiteColor,
                              fontWeight: FontWeight.bold,
                              fontSize: 14),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      });
}
