import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget loadingWidget(String msg) {
  return Container(
    child: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            msg,
          ),
          Container(
            margin: EdgeInsets.all(20),
            child: LinearProgressIndicator(
              backgroundColor: Colors.grey,
              minHeight: 6,
            ),
          ),
        ],
      ),
    ),
  );
}
