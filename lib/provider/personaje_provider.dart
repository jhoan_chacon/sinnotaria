import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sinnotaria/model/personaje_model.dart';

class PersonajeProvider with ChangeNotifier {
  List<PersonajeModel> personajesList = [];
  List<PersonajeModel> personajesListFiltro = [];
  List<String> statusList = [];
  List<int> temporadaList = [];
  bool _isLoading = true;
  bool _mostrarFiltros = false;
  String _selectedStatus = "";
  int _filtroActual = 0;

  bool get isLoading => this._isLoading;
  bool get mostrarFiltros => this._mostrarFiltros;
  String get selectedStatus => this._selectedStatus;
  int get paginaActual => this._filtroActual;

  set filtroActual(int valor) {
    this._filtroActual = valor;
    notifyListeners();
  }

  PersonajeProvider() {
    this.getPersonajes();
  }

  set selectedFiltro(String valor) {
    this._selectedStatus = valor;
    this._isLoading = true;
    this.getPersonajeStatus(valor);
    notifyListeners();
  }

  allPersonajes() {
    personajesListFiltro.clear();
    personajesListFiltro = personajesList;
    notifyListeners();
  }

  getPersonajes() async {
    List<String> tempGrupo = [];
    List<int> tempCapitulos = [];
    final url = Uri.parse('https://www.breakingbadapi.com/api/characters');
    final resp = await http.get(url);

    final personjesResponse = personajeModelFromJson(resp.body);
    this.personajesList.addAll(personjesResponse);

    ///Ciclo para obtener estatus unicos////
    for (var index = 0; index < personajesList.length; index++) {
      tempGrupo.add(personajesList[index].status);
    }
    var setGrupo = Set.of(tempGrupo);
    statusList.addAll(setGrupo);

    ///Ciclo para obtener temporadas unicos////
    for (var index = 0; index < personajesList.length; index++) {
      for (var x = 0; x < personajesList[index].appearance.length; x++) {
        tempCapitulos.add(personajesList[index].appearance[x]);
      }
    }
    var setTemporada = Set.of(tempCapitulos);
    temporadaList.addAll(setTemporada);

    _isLoading = false;
    _mostrarFiltros = true;
    personajesListFiltro.addAll(personajesList);
    notifyListeners();
  }

  getPersonajeStatus(String status) {
    List<PersonajeModel> tempPersonaje = [];
    for (var index = 0; index < personajesList.length; index++) {
      if (personajesList[index].status == status) {
        tempPersonaje.add(personajesList[index]);
      }
    }
    personajesListFiltro.clear();
    personajesListFiltro.addAll(tempPersonaje);
    tempPersonaje.clear();
    print(personajesListFiltro.length);
    notifyListeners();
  }

  getPersonajeTemporada(int temporada) {
    List<PersonajeModel> tempPersonaje = [];
    for (var index = 0; index < personajesList.length; index++) {
      if (personajesList[index].appearance.contains(temporada)) {
        tempPersonaje.add(personajesList[index]);
      }
    }
    personajesListFiltro.clear();
    personajesListFiltro.addAll(tempPersonaje);
    tempPersonaje.clear();
    print(personajesListFiltro.length);
    notifyListeners();
  }
}
