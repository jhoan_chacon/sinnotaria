import 'package:flutter/material.dart';

Color backgrounColor0 = Colors.grey.shade900;
Color backgrounColor1 = Color(0xff126a38);
Color whiteColor = Color(0xffFBFCFD);
Color blackColor = Colors.black;
Gradient principal = LinearGradient(colors: [
  Colors.transparent,
  Colors.transparent,
  Colors.transparent,
  blackColor,
  backgrounColor0,
]);
