import 'package:flutter/material.dart';
import 'package:sinnotaria/page/home_page.dart';

Map<String, WidgetBuilder> getAplicationRoutes() {
  return <String, WidgetBuilder>{
    "homePage": (_) => HomePage(),
  };
}
