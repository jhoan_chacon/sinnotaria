import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:sinnotaria/page/home_page.dart';
import 'package:sinnotaria/provider/personaje_provider.dart';
import 'package:sinnotaria/utilidades/routes.dart';
import 'package:sinnotaria/utilidades/styles.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => new PersonajeProvider()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'The Breaking Bad',
        //initialRoute: 'homePage',
        routes: getAplicationRoutes(),
        onGenerateRoute: (RouteSettings setiings) {
          return MaterialPageRoute(
              builder: (BuildContext context) => HomePage());
        },
        theme: ThemeData(
          primaryColor: backgrounColor0,
          bottomNavigationBarTheme: BottomNavigationBarThemeData(
              backgroundColor: backgrounColor0,
              selectedItemColor: Colors.white,
              unselectedItemColor: Colors.white60),
        ),
      ),
    );
  }
}
