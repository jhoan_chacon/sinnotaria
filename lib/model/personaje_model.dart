// To parse this JSON data, do
//
//     final personajeModel = personajeModelFromJson(jsonString);

import 'dart:convert';

List<PersonajeModel> personajeModelFromJson(String str) =>
    List<PersonajeModel>.from(
        json.decode(str).map((x) => PersonajeModel.fromJson(x)));

String personajeModelToJson(List<PersonajeModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class PersonajeModel {
  PersonajeModel({
    required this.charId,
    required this.name,
    required this.birthday,
    required this.occupation,
    required this.img,
    required this.status,
    required this.nickname,
    required this.appearance,
    required this.portrayed,
    required this.category,
    required this.betterCallSaulAppearance,
  });

  int charId;
  String name;
  String birthday;
  List<String> occupation;
  String img;
  String status;
  String nickname;
  List<int> appearance;
  String portrayed;
  String category;
  List<dynamic> betterCallSaulAppearance;

  factory PersonajeModel.fromJson(Map<String, dynamic> json) => PersonajeModel(
        charId: json["char_id"],
        name: json["name"],
        birthday: json["birthday"],
        occupation: List<String>.from(json["occupation"].map((x) => x)),
        img: json["img"],
        status: json["status"],
        nickname: json["nickname"],
        appearance: List<int>.from(json["appearance"].map((x) => x)),
        portrayed: json["portrayed"],
        category: json["category"],
        betterCallSaulAppearance: List<dynamic>.from(
            json["better_call_saul_appearance"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "char_id": charId,
        "name": name,
        "birthday": birthday,
        "occupation": List<dynamic>.from(occupation.map((x) => x)),
        "img": img,
        "status": status,
        "nickname": nickname,
        "appearance": List<dynamic>.from(appearance.map((x) => x)),
        "portrayed": portrayed,
        "category": category,
        "better_call_saul_appearance":
            List<dynamic>.from(betterCallSaulAppearance.map((x) => x)),
      };
}
