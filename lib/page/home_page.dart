import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:sinnotaria/model/personaje_model.dart';
import 'package:sinnotaria/page/filtros_page.dart';
import 'package:sinnotaria/provider/personaje_provider.dart';
import 'package:sinnotaria/widgets/lista_personaje_widget.dart';
import 'package:sinnotaria/widgets/loading_widget.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var listaPersonajes;
  TextEditingController _filter = new TextEditingController();
  Icon _searchIcon = new Icon(Icons.search);
  String _searchText = "";
  List<PersonajeModel> _personajeList = [];
  List<PersonajeModel> _searchResult = [];
  String msgListProducto = "Espere un momento";
  bool _firstSearch = true;

  _HomePageState() {
    _filter.addListener(() {
      if (_filter.text.isEmpty) {
        setState(() {
          _searchText = "";

          _firstSearch = true;
        });
      } else {
        setState(() {
          _searchText = _filter.text;
          _searchResult = _personajeList;
          _firstSearch = false;
          print(_searchText);
        });
      }
    });
  }

  void _searchPressed() {
    setState(() {
      _searchResult = _personajeList;
      _filter.clear();
    });
  }

  @override
  void initState() {
    super.initState();
    _personajeList.sort();
  }

  @override
  Widget build(BuildContext context) {
    final personajeProvider = Provider.of<PersonajeProvider>(context);
    _personajeList = personajeProvider.personajesListFiltro;
    _searchResult = _personajeList;
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: Center(
            child: Text(
          'Breaking Bad',
        )),
      ),
      body: Column(
        children: [
          Container(
            color: Theme.of(context).primaryColor,
            child: Padding(
              padding: EdgeInsets.all(2.0),
              child: Card(
                child: ListTile(
                  leading: _searchIcon,
                  title: TextField(
                    autofocus: false,
                    controller: _filter,
                    decoration: InputDecoration(
                        hintText: 'Buscar Producto', border: InputBorder.none),
                  ),
                  trailing: IconButton(
                    icon: Icon(Icons.cancel),
                    onPressed: () {
                      _filter.clear();
                      _searchPressed();
                    },
                  ),
                ),
              ),
            ),
          ),
          FiltrosPage(),
          Container(
            child: personajeProvider.isLoading == false
                ? Expanded(
                    child: _firstSearch
                        ? listaPersonajesWidget(context, _personajeList)
                        : _listaBusqueda())
                : Expanded(
                    child: Center(
                      child: loadingWidget(msgListProducto),
                    ),
                  ),
          ),
        ],
      ),
    );
  }

  //Funcion para obtener listas por el campo //
  Widget _listaBusqueda() {
    List<PersonajeModel> tempList = [];
    for (int i = 0; i < _searchResult.length; i++) {
      if (_searchResult[i]
              .name
              .toLowerCase()
              .contains(_searchText.toLowerCase()) ||
          _searchResult[i]
              .status
              .toLowerCase()
              .contains(_searchText.toLowerCase())) {
        tempList.add(_searchResult[i]);
      }
    }
    _searchResult = tempList;
    return listaPersonajesWidget(context, _searchResult);
  }
}
