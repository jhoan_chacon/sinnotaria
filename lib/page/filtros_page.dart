import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sinnotaria/provider/personaje_provider.dart';
import 'package:sinnotaria/utilidades/styles.dart';

class FiltrosPage extends StatefulWidget {
  @override
  _FiltrosPageState createState() => _FiltrosPageState();
}

class _FiltrosPageState extends State<FiltrosPage>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  @override
  // ignore: must_call_super
  Widget build(BuildContext context) {
    return Container(
      child: _ListaStatus(),
    );
  }
}

class _ListaStatus extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final personajeProvider = Provider.of<PersonajeProvider>(context);

    return Column(
      children: [
        Container(
          width: double.infinity,
          padding: EdgeInsets.symmetric(vertical: 0),

          decoration: BoxDecoration(
              color: Colors.grey.shade900,
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(20),
                  bottomRight: Radius.circular(20))),

          // height: 80.0,
          child: Visibility(
            visible: personajeProvider.mostrarFiltros,
            child: Column(
              children: [
                ListaEstatus(personajeProvider: personajeProvider),
                ListaTemporadas(personajeProvider: personajeProvider),
                BotonMostrarTodos(personajeProvider: personajeProvider),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
//Funcioon para obtener personajes por estatus//

class ListaEstatus extends StatelessWidget {
  const ListaEstatus({
    Key? key,
    required this.personajeProvider,
  }) : super(key: key);

  final PersonajeProvider personajeProvider;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: 15),
          child: Text(
            "Estatus del Personaje",
            style: TextStyle(
                color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold),
          ),
        ),
        Container(
          height: 70,
          padding: EdgeInsets.symmetric(vertical: 10),
          child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: personajeProvider.statusList.length,
              shrinkWrap: true,
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                  onTap: () => personajeProvider
                      .getPersonajeStatus(personajeProvider.statusList[index]),
                  child: Container(
                    width: 100,
                    margin: EdgeInsets.symmetric(horizontal: 16),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Container(
                      padding: EdgeInsets.all(8.0),
                      alignment: Alignment.center,
                      child: Text(
                        personajeProvider.statusList[index],
                        style: TextStyle(
                            color: backgrounColor1,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                );
              }),
        ),
      ],
    );
  }
}

//Funcioon para obtener personajes por temporadas//
class ListaTemporadas extends StatelessWidget {
  const ListaTemporadas({
    Key? key,
    required this.personajeProvider,
  }) : super(key: key);

  final PersonajeProvider personajeProvider;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: 15),
          child: Text(
            "Personaje por Temporada",
            style: TextStyle(
                color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold),
          ),
        ),
        Container(
          height: 70,
          padding: EdgeInsets.symmetric(vertical: 10),
          child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: personajeProvider.temporadaList.length,
              shrinkWrap: true,
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                  onTap: () => personajeProvider.getPersonajeTemporada(
                      personajeProvider.temporadaList[index]),
                  child: Container(
                    width: 100,
                    margin: EdgeInsets.symmetric(horizontal: 16),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Container(
                      padding: EdgeInsets.all(8.0),
                      alignment: Alignment.center,
                      child: Text(
                        "Temporada ${personajeProvider.temporadaList[index]}",
                        style: TextStyle(
                            color: backgrounColor1,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                );
              }),
        ),
      ],
    );
  }
}

//Boton para mostrar todos los Personajes//
class BotonMostrarTodos extends StatelessWidget {
  const BotonMostrarTodos({
    Key? key,
    required this.personajeProvider,
  }) : super(key: key);

  final PersonajeProvider personajeProvider;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => personajeProvider.allPersonajes(),
      child: Container(
        width: double.infinity,
        margin: EdgeInsets.only(left: 60),
        decoration: BoxDecoration(
          color: backgrounColor1,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20), bottomRight: Radius.circular(20)),
        ),
        child: Container(
          padding: EdgeInsets.all(8.0),
          alignment: Alignment.center,
          child: Text(
            "Mostrar Todos",
            style: TextStyle(color: whiteColor, fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
  }
}
